import sys
from time import *
from threading import Thread
import socket
import Randobot_gui as Rnd
import Randobot_style_sheet as Rando_sheet
import Randobot_bot as Bot
from PyQt5 import QtCore, QtGui, QtWidgets

# client configuration data
HOST = '192.168.43.217'  # The server's hostname or IP address '192.168.8.108' -RPi address 192.168.43.217 transfer
PORT = 19000      # The port used by the server
client_type = 'normal'
msg_server = ['TEXT']


# client handle
def receive_thread(sending_socket, Bot):
    while True:
        try:
            msg = sending_socket.recv(1024)
            msg_text = msg.decode('utf-8')
            msg_server.insert(0, msg_text)
            get_command(msg_text, Bot)
            print("Message from server: "+msg_text)
        except:
            print("Cant get message from server")
            break


def get_command(data, Bot):   # getting command from received string
    par = data.split(' ')
    if par[0] == 'Hcsr':
        Bot.sensors[0] = par[1]
        Bot.sensors[1] = par[2]
    elif par[0] == 'Temp':
        Bot.temperature = par[1]


def map_distance(distance, nr):
    try:
        bar1_value = (float(distance) / 200) * 100
        if bar1_value > 100:
            bar1_value = 100
        Bot.sensors_bar_value[nr] = bar1_value
    except:
        return Bot.sensors_bar_value[nr]
    return bar1_value


# class based on QMainWindow to redefine key Events
class MyWindow(QtWidgets.QMainWindow):
    def __init__(self, Bot):
        super(MyWindow, self).__init__()
        self.setWindowTitle('Operator panel')
        self.Bot = Bot

    def keyPressEvent(self, e):
        Bot.intervalFlag = True
        if e.key() == QtCore.Qt.Key_W:
            print("W")
            clickedButton[0] = 'W'
        elif e.key() == QtCore.Qt.Key_A:
            print("A")
            clickedButton[0] = 'A'
            self.Bot.direction = 'L'
        elif e.key() == QtCore.Qt.Key_D:
            print("D")
            clickedButton[0] = 'D'
            self.Bot.direction = 'R'
        elif e.key() == QtCore.Qt.Key_S:
            print("S")
            clickedButton[0] = 'S'
        elif e.key() == QtCore.Qt.Key_C:
            print("C")
            self.Bot.speed = 0
            clickedButton[0] = ''
        else:
            clickedButton[0] = ''

    def keyReleaseEvent(self, e):
        pass
        if e.key() in {QtCore.Qt.Key_D, QtCore.Qt.Key_A}:
            clickedButton[0] = ''
            if self.Bot.speed > 0:
                Bot.direction = 'F'
            elif self.Bot.speed < 0:
                Bot.direction = 'B'
            elif self.Bot.speed == 0:
                Bot.direction = 'S'


def speed_up(Bot):
    if Bot.speed < 100:
        Bot.speed += 5


def slow_down(Bot):
    if Bot.speed > -100:
        Bot.speed -= 5


def turn_left(Bot):
    if Bot.speed != 0:
        Bot.direction = 'L'


def turn_right(Bot):
    if Bot.speed != 0:
        Bot.direction = 'R'


def release_button(Bot):
    if Bot.speed > 0:
        Bot.direction = 'F'
    elif Bot.speed > 0:
        Bot.direction = 'B'
    else:
        Bot.direction = 'S'


def stop_bot(Bot):
    Bot.speed = 0


def turn_on_buzzer(Bot):
    Bot.client_socket.sendall("Buzzer 1".encode('utf-8'))


def turn_off_buzzer(Bot):
    Bot.client_socket.sendall("Buzzer 0".encode('utf-8'))


def turn_on_off_light(Bot, gui):
    if Bot.light == 0:
        Bot.light = 1
        Bot.client_socket.sendall("Light 1".encode('utf-8'))
        Rando_sheet.set_positive_light(gui.light_ind)
    else:
        Bot.light = 0
        Bot.client_socket.sendall("Light 0".encode('utf-8'))
        Rando_sheet.set_negative_light(gui.light_ind)


def turn_on_off_coil(Bot, gui):
    if Bot.coil == 0:
        Bot.coil = 1
        Bot.client_socket.sendall("Coil 1".encode('utf-8'))
        Rando_sheet.set_positive_light(gui.coil_ind)
    else:
        Bot.coil = 0
        Bot.client_socket.sendall("Coil 0".encode('utf-8'))
        Rando_sheet.set_negative_light(gui.coil_ind)


def refresh(gui, Bot):      # function connected to timer
    # Bot speed, control by keys
    if Bot.speed < 100 and clickedButton[0] == 'W' and Bot.intervalFlag:
        Bot.speed += 5
        Bot.intervalFlag = False
    if Bot.speed > -100 and clickedButton[0] == 'S' and Bot.intervalFlag:
        Bot.speed -= 5
        Bot.intervalFlag = False
    # Bot direction
    if Bot.speed == 0:
        Bot.direction = 'S'
    elif Bot.speed > 0 and Bot.direction not in {'R', 'L'}:
        Bot.direction = 'F'
    elif Bot.speed < 0 and Bot.direction not in {'R', 'L'}:
        Bot.direction = 'B'

    if Bot.direction == 'S':
        Rando_sheet.set_zero_indicator_wheel(gui.BR_ind)
        Rando_sheet.set_zero_indicator_wheel(gui.BL_ind)
        Rando_sheet.set_zero_indicator_wheel(gui.FL_ind)
        Rando_sheet.set_zero_indicator_wheel(gui.FR_ind)
    elif Bot.direction == 'F':
        Rando_sheet.set_positive_indicator_wheel(gui.BR_ind)
        Rando_sheet.set_positive_indicator_wheel(gui.BL_ind)
        Rando_sheet.set_positive_indicator_wheel(gui.FL_ind)
        Rando_sheet.set_positive_indicator_wheel(gui.FR_ind)
    elif Bot.direction == 'B':
        Rando_sheet.set_negative_indicator_wheel(gui.BR_ind)
        Rando_sheet.set_negative_indicator_wheel(gui.BL_ind)
        Rando_sheet.set_negative_indicator_wheel(gui.FL_ind)
        Rando_sheet.set_negative_indicator_wheel(gui.FR_ind)
    elif Bot.direction == 'L':
        Rando_sheet.set_positive_indicator_wheel(gui.BR_ind)
        Rando_sheet.set_negative_indicator_wheel(gui.BL_ind)
        Rando_sheet.set_negative_indicator_wheel(gui.FL_ind)
        Rando_sheet.set_positive_indicator_wheel(gui.FR_ind)
    elif Bot.direction == 'R':
        Rando_sheet.set_negative_indicator_wheel(gui.BR_ind)
        Rando_sheet.set_positive_indicator_wheel(gui.BL_ind)
        Rando_sheet.set_positive_indicator_wheel(gui.FL_ind)
        Rando_sheet.set_negative_indicator_wheel(gui.FR_ind)

    if Bot.speed > 0:    # set progress bar appearance based on speed value
        Rando_sheet.set_positive_progressbar(gui.progressBar)
    else:
        Rando_sheet.set_negative_progressbar(gui.progressBar)

    gui.progressBar.setValue(abs(Bot.speed))
    gui.ServerMsg.setText(msg_server[0])
    gui.ServerMsg.adjustSize()

    # send data about motor to server
    motor_data = 'Motor'+' '+Bot.direction+' '+str(Bot.set_speed())
    Bot.client_socket.sendall(motor_data.encode('utf-8'))

    # show data about distance
    # labels
    gui.FrontDistLabel.setText(str(Bot.sensors[0]) + ' cm')
    gui.BackDistLabel.setText(str(Bot.sensors[1]) + ' cm')
    # bars
    gui.FrontDistBar.setValue(map_distance(Bot.sensors[0], 0))
    gui.BackDistBar.setValue(map_distance(Bot.sensors[1], 1))

    # temperature measurement
    gui.temperature.setText(Bot.temperature+' C')
    gui.temperature.adjustSize()
    gui.frame_2.adjustSize()

    # servo control
    Bot.bot_node[0] = gui.BaseSlider.value()
    Bot.bot_node[1] = gui.FirstNodeSlider.value()
    Bot.bot_node[2] = gui.SecondNodeSlider.value()
    gui.Angle0.setText(str(Bot.bot_node[0])+' deg')
    gui.Angle1.setText(str(Bot.bot_node[1])+' deg')
    gui.Angle2.setText(str(Bot.bot_node[2])+' deg')
    # send data about servo angle to server
    servo_data = 'Servo' + ' ' + str(Bot.bot_node[0]) + ' ' + str(Bot.bot_node[1]) + ' ' + str(Bot.bot_node[2])
    Bot.client_socket.sendall(servo_data.encode('utf-8'))


def sendToServer(c_socket, data):
    c_socket.sendall(data.encode('utf-8'))


def addTimer(gui, Bot):   # function to add timer to gui
    ui.timer = QtCore.QTimer()
    gui.timer.timeout.connect(lambda: refresh(gui, Bot))
    gui.timer.setInterval(100)
    gui.timer.start()


clickedButton = ['']  # define which button was clicked last time
intervalFlag = [False]  # setting after key event, resetting in refresh()
speed = [0]           # speed

if __name__ == "__main__":
    # client
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    client_socket.sendall(client_type.encode('utf-8'))  # sending type of client socket to server

    # Bot def
    Bot = Bot.Bot()
    Bot.client_socket = client_socket

    reader_thread = Thread(target=receive_thread, args=(client_socket, Bot,))
    reader_thread.start()

    app = QtWidgets.QApplication(sys.argv)
    RandoBot = MyWindow(Bot)
    ui = Rnd.Ui_RandoBot()

    addTimer(ui, Bot)
    ui.setupUi(RandoBot)

    ui.Send.clicked.connect(lambda: sendToServer(client_socket, ui.textEdit.toPlainText()))

    # Bot speed, control by buttons
    ui.SpeedButton.pressed.connect(lambda: speed_up(Bot))
    ui.SlowButton.pressed.connect(lambda: slow_down(Bot))
    ui.LeftButton.pressed.connect(lambda: turn_left(Bot))
    ui.RighButton.pressed.connect(lambda: turn_right(Bot))
    ui.LeftButton.released.connect(lambda: release_button(Bot))
    ui.RighButton.released.connect(lambda: release_button(Bot))
    ui.StopButton.pressed.connect(lambda: stop_bot(Bot))
    ui.Buzzer.pressed.connect(lambda: turn_on_buzzer(Bot))
    ui.Buzzer.released.connect(lambda: turn_off_buzzer(Bot))
    ui.Light.pressed.connect(lambda: turn_on_off_light(Bot, ui))
    ui.Coil.pressed.connect(lambda: turn_on_off_coil(Bot, ui))

    RandoBot.show()
    sys.exit(app.exec_())