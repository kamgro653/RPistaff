import pwmhat as PWM
import motor_dualMax as Mot
from time import*
import socket
import sys
import errno
from threading import Thread
import sharp_MCP as Sharp
import hcsr04 as hcsr
import w1thermsensor

# motors configuration
Driver1 = Mot.MotorDriverConf(27, 22)
Driver1.enable()

pwm1= PWM.PWM(800)          # pwm declaration
pwm1.addChannel("motor", 1)     # left-front wheel
pwm1.addChannel("motor", 3)     # left-back wheel
pwm1.addChannel("motor", 0)     # right-front wheel
pwm1.addChannel("motor", 2)     # right-back wheel
pwm1.CH[0].dir_set(6)
pwm1.CH[1].dir_set(5)
pwm1.CH[2].dir_set(13)
pwm1.CH[3].dir_set(12)

pwm1.addChannel("default", 4)   # buzzer TEST
pwm1.CH[4].mod(0)

t_sensor = w1thermsensor.W1ThermSensor()   # temperature sensor

# distance sensors configuration
Sensor1 = hcsr.Hcsr04()
Sensor1.connection(19, 16)
Sensor2 = hcsr.Hcsr04()
Sensor2.connection(20, 21)

# server configuration
HOST = '192.168.43.217'  # Standard loopback interface address / '192.168.8.108' -RPi address '127.0.0.1' - local 192.168.43.217 - transfer
PORT = 19000       # Port to listen on (non-privileged ports are > 1023)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()
client_msg = ['']
reader_socket = []


def waiting_connection():
        conn, addr = server_socket.accept()
        print('Connected by', addr)
        client_socket = conn
        data = client_socket.recv(1024)
        data_text = data.decode("utf-8")
        if data_text == 'normal':
            client_thread = Thread(target=client_handle, args=(client_socket, addr,))
            client_thread.start()
            sending_thread = Thread(target=send_client, args=(client_socket, addr,))
            sending_thread.start()
        elif data_text == 'reader':
            reader_sck = client_socket
            if len(reader_socket) != 0:
                reader_socket.clear()
            reader_socket.append(reader_sck)
        else:
            print('Wrong client type')


def temperature_control(client_socket, sensor):
    while True:
        try:
            temp = round(sensor.get_temperature(), 2)
            temp_msg = 'Temp' + ' ' + str(temp)
            print(str(temp))
            client_socket.sendall(temp_msg.encode('utf-8'))
            sleep(2)
        except:
            pass


def send_client(client_socket, addr):
    interval_time = 0.4     # delay between sending commands
    temp_time = perf_counter() + interval_time
    temp_thread = Thread(target=temperature_control, args=(client_socket, t_sensor,))
    temp_thread.start()
    while True:
        try:
            # toDo send data from server
            # msg = input("")
            if (temp_time - perf_counter()) < 0:
                temp_time = perf_counter() + interval_time
                send_command(client_socket)
            # client_msg[0] = msg
            try:
                pass        # temporary
                # broadcast_reader(reader_socket[0], 'Server', msg)
            except:
                pass
        except:
            print("This client is already disconnected, address: " + str(addr))
            break


def client_handle(client_socket, addr):
    while True:
        try:
            data = client_socket.recv(1024)
            data_text = data.decode('utf-8')
            get_command(data_text)
            print("Message from operator: "+data_text)
            try:
                broadcast_reader(reader_socket[0], 'Operator', data_text)
            except:
                pass
        except:
            print("Client (address: " + str(addr) + ") has been disconnected")
            break


def get_command(data):   # getting command from received string
    par = data.split(' ')
    if par[0] == 'Motor':
        motor_conf(par[1], par[2])
    elif par[0] == 'Buzzer':
        if par[1] == '1':
            turn_on_buzzer()
        elif par[1] == '0':
            turn_off_buzzer()


def send_command(ssocket):     # sending data about eg distance to client
    comm = 'Hcsr' + ' ' + str(round(Sensor1.distanceCheck(), 2)) + ' ' + str(round(Sensor2.distanceCheck(), 2)) + ' ' \
           + "end"
    ssocket.sendall(comm.encode('utf-8'))


def motor_conf(direction, speed):
    try:
        if direction in {'S', 'F', 'B'}:                                            # straight, back, stop
            pwm1.CH[0].set_speed(float(speed))
            pwm1.CH[1].set_speed(float(speed))
            pwm1.CH[2].set_speed(float(speed))
            pwm1.CH[3].set_speed(float(speed))
        elif (direction == 'L' and float(speed) > 0) or (direction == 'R' and float(speed) < 0):  # turn left
            pwm1.CH[0].set_speed(float(speed))
            pwm1.CH[1].set_speed(-float(speed))
            pwm1.CH[2].set_speed(float(speed))
            pwm1.CH[3].set_speed(-float(speed))
        elif (direction == 'R' and float(speed) > 0) or (direction == 'L' and float(speed) < 0):  # turn right
            pwm1.CH[0].set_speed(-float(speed))
            pwm1.CH[1].set_speed(float(speed))
            pwm1.CH[2].set_speed(-float(speed))
            pwm1.CH[3].set_speed(float(speed))
        print(speed)
    except:
        pass


def turn_on_buzzer():
    pwm1.CH[4].mod(35500)


def turn_off_buzzer():
    pwm1.CH[4].mod(0)


def broadcast_reader(reader_socket, sender, msg):
    msgrd = sender + ": "+msg
    reader_socket.sendall(msgrd.encode('utf-8'))


def main():
    print("Server listening for connection...")
    while True:
        waiting_connection()


if __name__ == "__main__":
    main()