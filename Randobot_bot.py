class Bot:

    speed = 0
    direction = 'S'
    direction_list = ['F', 'B', 'L', 'R', 'S']  # possible directions (Forward Back Left Right Stop)
    client_socket = 0
    sensors = [999, 999]
    sensors_bar_value = [100, 100]
    temperature = '0'
    bot_node = ['0', '0', '0']
    light = 0
    coil = 0

    def set_speed(self):
        return self.speed*1.6   # map to be compatible with motor_dualMax

    intervalFlag = False  # interval Flag, to handle keyPress Event
