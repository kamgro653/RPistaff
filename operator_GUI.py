from PyQt5 import QtCore, QtGui, QtWidgets, QtCore
import socket
import errno
import sys
from threading import Thread
from time import *

# client configuration data
HOST = '127.0.0.1'  # The server's hostname or IP address '192.168.8.108' -RPi address
PORT = 19000      # The port used by the server
client_type = 'normal'
msg_server = ['TEXT']


# client handle
def receive_thread(sending_socket):
    while True:
        try:
            msg = sending_socket.recv(1024)
            msg_text = msg.decode('utf-8')
            msg_server.insert(0, msg_text)
            print("Message from server: "+msg_text)
        except:
            print("Cant get message from server")
            break


def send1(c_socket):
    msg = 'Hello'
    c_socket.sendall(msg.encode('utf-8'))


def send2(c_socket):
    msg = 'Roger That'
    c_socket.sendall(msg.encode('utf-8'))


def send3(c_socket):
    msg = 'WOZAP'
    c_socket.sendall(msg.encode('utf-8'))


# gui
class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.setWindowTitle('Operator panel')

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_W:
            print("W")
        elif e.key() == QtCore.Qt.Key_A:
            print("A")
        elif e.key() == QtCore.Qt.Key_D:
            print("D")
        elif e.key() == QtCore.Qt.Key_S:
            print("S")


class Ui_MainWindow(object):
    def __init__(self, c_socket):
        self.c_socket = c_socket

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.setInterval(100)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.button1 = QtWidgets.QPushButton(self.centralwidget)
        self.button1.setGeometry(QtCore.QRect(10, 40, 181, 71))
        self.button1.setObjectName("button1")
        self.button1.clicked.connect(lambda: send1(self.c_socket))
        self.label1 = QtWidgets.QLabel(self.centralwidget)
        self.label1.setGeometry(QtCore.QRect(10, 140, 151, 71))
        self.label1.setObjectName("label1")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 190, 81, 16))
        self.label.setObjectName("label")
        self.button2 = QtWidgets.QPushButton(self.centralwidget)
        self.button2.setGeometry(QtCore.QRect(200, 40, 181, 71))
        self.button2.setObjectName("button2")
        self.button2.clicked.connect(lambda: send2(self.c_socket))
        self.button3 = QtWidgets.QPushButton(self.centralwidget)
        self.button3.setGeometry(QtCore.QRect(390, 40, 181, 71))
        self.button3.setObjectName("button3")
        self.button3.clicked.connect(lambda: send3(self.c_socket))
        self.button4 = QtWidgets.QPushButton(self.centralwidget)
        self.button4.setGeometry(QtCore.QRect(590, 40, 181, 71))
        self.button4.setObjectName("button4")
        self.button4.setText('Force Refresh')
        self.button4.clicked.connect(lambda: self.refresh())
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 26))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuFile.menuAction())
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    # def keyPressEvent(self, e):
    #     print("event", e)
    #     if e.key() == QtCore.Qt.Key_Space:
    #         print(' space')
    #     elif e.key() == QtCore.Qt.Key_Enter:
    #         print(' enter')

    def retranslateUi(self, MainWindow):
        self.timer.start()
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Operator Panel"))
        self.button1.setText(_translate("MainWindow", "Send \'Hello\'"))
        self.label1.setText(_translate("MainWindow", "From server:"))
        self.label.setText(_translate("MainWindow", msg_server[0]))
        self.button2.setText(_translate("MainWindow", "Send \'Roger That\'"))
        self.button3.setText(_translate("MainWindow", "Send \'WOZAP\'"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))

    def refresh(self):
        self.label.setText(msg_server[0])
        self.label.adjustSize()


if __name__ == "__main__":
    # client
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    client_socket.sendall(client_type.encode('utf-8'))  # sending type of client socket to server
    reader_thread = Thread(target=receive_thread, args=(client_socket,))
    reader_thread.start()

    # gui
    app = QtWidgets.QApplication(sys.argv)
    MyWindow = MyWindow()
    # MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow(client_socket)
    ui.setupUi(MyWindow)
    MyWindow.show()
    # refresh thread

    # end program
    sys.exit(app.exec_())

