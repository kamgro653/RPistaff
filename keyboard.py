from time import *
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

class Keyboard:
    def __init__(self,b1,b2,b3,b4):
        GPIO.setup(b1, GPIO.IN)
        GPIO.setup(b2, GPIO.IN)
        GPIO.setup(b3, GPIO.IN)
        GPIO.setup(b4, GPIO.IN)
        self.key_nr = [0, b1, b2, b3, b4]

    def charAssign(self, set_nr):
        self.char_act_st = self.char_st[set_nr]
        self.char_act_adv = self.char_adv[set_nr]

    def checkState(self,key_nr):
        if GPIO.input(key_nr) == 1:
            return 1
        else:
            return 0
    token = 0
    char_act_st = []
    char_act_adv = []
    char_st = []
    char_adv = []

    char_st.append([0, 'y', 'o', 'u', 'r'])
    char_adv.append([0, 'm', '\n', 'k', 'g'])
    char_st.append([0, '1', '2', '3', '4'])
    char_adv.append([0, '5', '6', '7', 'ok'])

    temp = 0
    mark = [0, 0, 0, 0, 0]

    def advCheck(self,key_nr,number):
        if GPIO.input(key_nr) == 1 and self.mark[number] == 0 and self.token == 0:
            self.token = 1
            self.mark[number] = 1
            self.temp = perf_counter()
        if GPIO.input(key_nr) == 0 and self.mark[number] == 1:
            self.token = 0
            self.mark[number] = 0
            delay = perf_counter() - self.temp
            if delay > 0.5:
                return self.char_act_adv[number], 1
            else:
                return self.char_act_st[number], 1
        else:
            return 0, 0

