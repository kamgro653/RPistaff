import LCDState as LCDSt
import pass_term as password
import keyboard as keyboard
import obstacle_game as game1
import board
import digitalio
import adafruit_character_lcd.character_lcd as characterlcd

lcd_columns = 16            # init module
lcd_rows = 2

lcd_rs = digitalio.DigitalInOut(board.D21)
lcd_en = digitalio.DigitalInOut(board.D20)
lcd_d4 = digitalio.DigitalInOut(board.D22)
lcd_d5 = digitalio.DigitalInOut(board.D23)
lcd_d6 = digitalio.DigitalInOut(board.D24)
lcd_d7 = digitalio.DigitalInOut(board.D25)

lcd = characterlcd.Character_LCD_Mono(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6,   # init LCD
lcd_d7, lcd_columns, lcd_rows)

lcd.clear()

keyboard1 = keyboard.Keyboard(19, 16, 13, 12)   # init keyboard
keyboard1.charAssign(1)

button = [0, 0, 0, 0]
password = password.PassTerminal("data.txt")    # init password module

obstacle_game = game1.obstacleGame(lcd)

ProgramState = LCDSt.ProgramState(lcd, button, keyboard1, password, obstacle_game)     # init main module

while True:
    ProgramState.main_state()   # infinite program loop
