import socket
import sys
import errno
from threading import Thread
from time import *


HOST = '127.0.0.1'  # Standard loopback interface address (localhost) '192.168.8.108' -RPi address '127.0.0.1' - local
PORT = 19000       # Port to listen on (non-privileged ports are > 1023)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()
client_msg = ['']
reader_socket = []


def waiting_connection():
        conn, addr = server_socket.accept()
        print('Connected by', addr)
        client_socket = conn
        data = client_socket.recv(1024)
        data_text = data.decode("utf-8")
        if data_text == 'normal':
            client_thread = Thread(target=client_handle, args=(client_socket, addr,))
            client_thread.start()
            sending_thread = Thread(target=send_client, args=(client_socket, addr,))
            sending_thread.start()
        elif data_text == 'reader':
            reader_sck = client_socket
            if len(reader_socket) != 0:
                reader_socket.clear()
            reader_socket.append(reader_sck)
        else:
            print('Wrong client type')


def send_client(client_socket, addr):
    while True:
        try:
            msg = input("")
            client_socket.sendall(msg.encode('utf-8'))
            client_msg[0] = msg
            try:
                broadcast_reader(reader_socket[0], 'Server', msg)
            except:
                pass
        except:
            print("This client is already disconnected, address: " + str(addr))
            break


def client_handle(client_socket, addr):
    while True:
        try:
            data = client_socket.recv(1024)
            data_text = data.decode("utf-8")
            print("Message from operator: "+data_text)
            try:
                broadcast_reader(reader_socket[0], 'Operator', data_text)
            except:
                pass
        except:
            print("Client (address: " + str(addr) + ") has been disconnected")
            break


def broadcast_reader(reader_socket, sender, msg):
    msgrd = sender + ": "+msg
    reader_socket.sendall(msgrd.encode('utf-8'))


def main():
    print("Server listening for connection...")
    while True:
        waiting_connection()


if __name__ == "__main__":
    main()
