#!/usr/bin/env python3

import socket
import errno
import sys
from threading import Thread

HOST = '192.168.8.108'  # The server's hostname or IP address '192.168.8.108' -RPi address
PORT = 19000      # The port used by the server
client_type = 'normal'


def receive_thread(sending_socket):
    while True:
        try:
            msg = sending_socket.recv(1024)
            msg_text = msg.decode('utf-8')
            print("Message from server: "+msg_text)
        except:
            print("Cant get message from server")
            break


def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    client_socket.sendall(client_type.encode('utf-8'))  # sending type of client socket to server
    reader_thread = Thread(target=receive_thread, args=(client_socket,))
    reader_thread.start()

    while True:
        try:
            msg = input('')
            client_socket.sendall(msg.encode('utf-8'))
        except:
            print("client disconnected")
            sys.exit()


if __name__ == "__main__":
    main()



