from time import *
from random import *


class obstacleGame:
    def __init__(self, lcd):
        self.LCD = lcd
        car = self.LCD.create_char(0, [0, 12, 19, 29, 29, 19, 12, 0])   # custom char from \x00 to \x07
        shield = self.LCD.create_char(1, [0, 0, 4, 10, 10, 4, 0, 0])
        skin1 = self.LCD.create_char(2, [0, 4, 2, 1, 1, 2, 4, 0])
        skin2 = self.LCD.create_char(3, [0, 0, 27, 4, 4, 27, 0, 0])
        slow = self.LCD.create_char(4, [31, 17, 10, 4, 4, 10, 17, 31])
        self.temp = 0
        self.score = 0
        self.char_pos = 1
        self.tick_rd = 0
        self.render = ['', '']  # display "messages" during game - 1 line
        self.game_tick = 0.5
        self.state = '0'  # state of game + inter
        self.game_state = '0'  # pure state of game
        self.action = '0'  # data send from main system to interact with game
        self.distance = []   # table of distance of objects
        self.objects = []    # table of object type
        self.place = []    # line of object
        self.pop_cor = 0   # correction after pop
        self.char_dict = {
            'car': car,
            'shield': shield,
            'skin1': skin1,
            'skin2': skin2,
            'slow': slow
        }
        self.skin_chosen = '1'
        self.skin_dict = {
            '1': '\x02',
            '2': '\x03'
        }

        self.state_dict = {
          '0': self.start_screen,
          '1': self.game,
          '2': self.change_skin,
          '3': self.exit
        }

        self.game_state_dict = {
            '0': self.game_start_screen,
            '1': self.game_play,
            '2': self.game_end_screen,
            '3': self.game_try_again,
        }

        self.action_list = ['1', '2', '3', '4']  # list of default actions

    def delay(self):
        self.LCD.clear()
        self.action = '0'
        sleep(0.4)

    def start_screen(self):
        if self.state == '0':
            self.LCD.message = "1-Start 3-exit \n2-Zmien skin"
            for i in self.action_list:
                if i == self.action:
                    self.state = self.action
                    self.delay()
        else:
            self.state_dict[self.state]()

    def change_skin(self):
        self.LCD.message = "Wybierz skin:\n 1-\x02 2-\x03"
        if self.action == '1':
            self.LCD.clear()
            self.skin_chosen = '1'
            self.LCD.message = "Wybrano skin\nnr 1"
            self.state = '0'
            self.delay()
        elif self.action == '2':
            self.LCD.clear()
            self.skin_chosen = '2'
            self.LCD.message = "Wybrano skin\nnr 2"
            self.state = '0'
            self.delay()

    def game(self):
        self.game_state_dict[self.game_state]()

    def game_step(self):
        if self.tick_rd == 1:
            self.temp = perf_counter()
            self.tick_rd = 0
        if self.temp + self.game_tick < perf_counter():
            self.tick_rd = 1

    def game_start_screen(self):
        for i in range(3):
            self.LCD.message = "Start in: "+str(3-i) + "\nGood Luck!"
            sleep(1)
        self.game_state = '1'
        self.delay()

    def game_play(self):
        # toDO
        self.game_step()
        if self.tick_rd == 1:
            self.LCD.clear()
            self.logic()
            self.render_game()

    def game_end_screen(self):
        self.LCD.message = "GAME OVER\nYour got:" + str(self.score) + "pts"
        sleep(3)
        self.game_state = '3'

    def game_try_again(self):
        self.LCD.message = "Try again?\n 1-Yes 2-No"
        if self.action == '1':
            self.game_state = '0'
            self.delay()
        elif self.action == '2':
            self.state = '0'
            self.delay()

    def exit(self):
        self.state = 'end'
        self.LCD.clear()

    def render_game(self):       # display game
        # TOdo
        self.render[0] = ''
        self.render[1] = ''   # clear display
        if self.char_pos == 1:
            self.render[0] += self.skin_dict[self.skin_chosen]
            self.render[1] += ' '
        elif self.char_pos == 2:
            self.render[1] += self.skin_dict[self.skin_chosen]        # render player character
            self.render[0] += ' '

        self.render_env()                                           # render environment

        self.LCD.message = self.render[0] + '\n' + self.render[1]

    def logic(self):        # calculation, logic of game
        # TODO
        if self.action == '1':
            self.char_pos = 1
        elif self.action == '2':
            self.char_pos = 2

    def render_env(self):
        spawn = randint(0, 5)
        pos = randint(1, 2)
        if spawn == 0:                  # try to spawn new object
            self.objects.append('car')
            self.distance.append(15)
            self.place.append(pos)
        temp = 0
        for i in self.objects:          # change distance of objects
            if i == 'car':
                self.distance[temp] -= 1
            temp += 1

        # if self.pop_cor == 1:       # correct distance if pop was done in the last step
        #     self.render[0] += ' '
        #     self.render[1] += ' '
        #     self.pop_cor = 0

        temp = 0
        for i in self.distance:         # render lines
            if temp == 0 and self.place[temp] == 1:
                self.render[0] += ' '*self.distance[temp]+'\x00'
                self.render[1] += ' ' * (self.distance[temp]+1)
            elif temp == 0 and self.place[temp] == 2:
                self.render[1] += ' '*self.distance[temp]+'\x00'
                self.render[0] += ' ' * (self.distance[temp]+1)
            elif temp != 0 and self.place[temp] == 1:
                self.render[0] += ' ' * (self.distance[temp] - self.distance[temp-1]-1) + '\x00'
                self.render[1] += ' ' * (self.distance[temp] - self.distance[temp - 1])
            elif temp != 0 and self.place[temp] == 2:
                self.render[1] += ' ' * (self.distance[temp] - self.distance[temp - 1]-1) + '\x00'
                self.render[0] += ' ' * (self.distance[temp] - self.distance[temp - 1])
            temp += 1

        temp = 0
        for each in self.distance:         # delete object if it past
            if self.distance[temp] == 0:
                self.distance.pop(temp)
                self.objects.pop(temp)
                self.place.pop(temp)
                # self.pop_cor = 1
        temp += 1











