from time import *


class ProgramState:
    def __init__(self, lcd, button, keyboard, password, obstacle_game):
        self.state = '2'    # init state is login
        self.LCD = lcd      # lcd object from declaration in main
        self.text = ''
        self.keyboard = keyboard
        self.button = button
        self.key_enable = 1
        self.accept = 0
        self.password = password
        self.possible_states = ['login', 'main', 'calc', 'try_login', 'delay',
                                'obstacle_game']
        self.state_list = ['0', '1', '2', '3', '4']
        self.state_dict = {        # dictionary of states
            '0': self.main_state,
            '1': self.calculator,
            '2': self.login,
            '3': self.try_login,
            '4': self.obstacle_gm,
        }
        self.calc_state = 0
        self.calc_a = ' '
        self.calc_b = ' '
        self.calc_c = ' '
        self.calc_stage = 0
        self.calc_state_list = ['0', '1', '2', '3', '4']
        self.calc_func_dict = {         # dictionary of calculator states
            '1': self.add,
            '2': self.subtract,
            '3': self.multiply,
            '4': self.divide,
        }
        self.obstacle_game = obstacle_game
        self.action_list = ['1', '2', '3', '4']

    def main_state(self):           # main state
        if self.state == '0':
            self.LCD.message = "Co zrobic?\n1-c1 2-lgt 4-gm"
            self.writing_phase()
            for i in self.state_list:
                if i == self.text:
                    self.state = self.text
                    self.delay()
        self.state_dict[self.state]()

    def go_main(self):  # function that set back state to '0'
        self.state = '0'

    def delay(self):        # blank state to wait till next
        sleep(0.4)
        self.calc_a = ''    # reset previously changed parameters
        self.calc_b = ''
        self.calc_c = ''
        self.calc_stage = 0
        self.text = ''
        self.LCD.clear()

    def login(self):        # login phase
        self.writing_phase()
        self.LCD.message = "Wpisz haslo:\n" + self.text
        if self.accept == 1:
            self.accept = 0
            check = self.password.tryPassword(self.text)
            self.LCD.message = check[0]
            if check[1] == 1:
                self.state = '0'    # go to main state
            else:
                self.state = '3'    # try to re-enter pass
            self.delay()

    def try_login(self):    # try again to login state
        self.LCD.message = "Ponowic?\n nacisnij ok"     # not finished
        self.writing_phase()
        if self.accept == 1:
            self.accept = 0
            self.state = '2'
            self.delay()

    def calculator(self):
        if self.calc_state == 0:
            self.LCD.message = "Wybierz dzial:\n1+2-3*4/ok_end"
            self.writing_phase()
            if self.accept == 1:
                self.accept = 0
                self.state = '0'
                self.delay()
            if self.text != '':
                number = self.text
                for i in self.calc_state_list:
                    if i == number and i != '0':
                        print("Element Exists")  # make dictionary to redirect to other state if proper symbol
                        self.calc_state = number
                        self.delay()
        else:
            self.calc_func_dict[self.calc_state]()      # do func from dictionary with null args

    def add(self):
        self.LCD.message = "Dzialanie: \n" + self.calc_a + '+' + self.calc_b + '=' + str(self.calc_c)
        self.writing_phase()
        if self.calc_stage == 0:
            self.calc_a = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_stage = 1
                self.text = ''
        if self.calc_stage == 1:
            self.calc_b = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_c = float(self.calc_a) + float(self.calc_b)
                self.calc_stage = 2
        if self.calc_stage == 2 and self.accept:
            self.accept = 0
            self.calc_state = 0  # go back to calculator
            self.delay()

    def subtract(self):
        self.LCD.message = "Dzialanie: \n" + self.calc_a + '-' + self.calc_b + '=' + str(self.calc_c)
        self.writing_phase()
        if self.calc_stage == 0:
            self.calc_a = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_stage = 1
                self.text = ''
        if self.calc_stage == 1:
            self.calc_b = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_c = float(self.calc_a) - float(self.calc_b)
                self.calc_stage = 2
        if self.calc_stage == 2 and self.accept:
            self.accept = 0
            self.calc_state = 0  # go back to calculator
            self.delay()

    def multiply(self):
        self.LCD.message = "Dzialanie: \n" + self.calc_a + '*' + self.calc_b + '=' + str(self.calc_c)
        self.writing_phase()
        if self.calc_stage == 0:
            self.calc_a = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_stage = 1
                self.text = ''
        if self.calc_stage == 1:
            self.calc_b = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_c = float(self.calc_a) * float(self.calc_b)
                self.calc_stage = 2
        if self.calc_stage == 2 and self.accept:
            self.accept = 0
            self.calc_state = 0  # go back to calculator
            self.delay()

    def divide(self):
        self.LCD.message = "Dzialanie: \n" + self.calc_a + '/' + self.calc_b + '=' + str(self.calc_c)
        self.writing_phase()
        if self.calc_stage == 0:
            self.calc_a = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_stage = 1
                self.text = ''
        if self.calc_stage == 1:
            self.calc_b = self.text
            if self.accept == 1:
                self.accept = 0
                self.calc_c = float(self.calc_a) / float(self.calc_b)
                self.calc_stage = 2
        if self.calc_stage == 2 and self.accept:
            self.accept = 0
            self.calc_state = 0  # go back to calculator
            self.delay()

    def obstacle_gm(self):
        self.writing_phase()
        self.obstacle_game.start_screen()
        if self.obstacle_game.state == 'end':
            self.obstacle_game.state = '0'
            self.state = '0'
        for i in self.action_list:
            if i == self.text:
                self.obstacle_game.action = self.text
        self.text = ''

    def writing_phase(self):
        # allowing user to write with declared in main set of chars, should be in loop on other thread
        for b in range(4):
            # checking pressing buttons functions
            self.button[b] = self.keyboard.advCheck(self.keyboard.key_nr[b + 1], b + 1)
        for x in range(4):
            if self.button[x][1] != 0 and self.key_enable == 1 and self.button[x][0] != 'ok':
                self.text = self.text + self.button[x][0]
            elif self.button[x][0] == 'ok':
                self.accept = 1

