from time import *
import pass_term as password
import keyboard as keyboard
import board
import digitalio
import adafruit_character_lcd.character_lcd as characterlcd

# Modify this if you have a different sized character LCD
lcd_columns = 16
lcd_rows = 2

# compatible with all versions of RPI as of Jan. 2019
# v1 - v3B+
lcd_rs = digitalio.DigitalInOut(board.D21)
lcd_en = digitalio.DigitalInOut(board.D20)
lcd_d4 = digitalio.DigitalInOut(board.D22)
lcd_d5 = digitalio.DigitalInOut(board.D23)
lcd_d6 = digitalio.DigitalInOut(board.D24)
lcd_d7 = digitalio.DigitalInOut(board.D25)

lcd = characterlcd.Character_LCD_Mono(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6,
lcd_d7, lcd_columns, lcd_rows)

lcd.clear()

keyboard1 = keyboard.Keyboard(19, 16, 13, 12)
keyboard1.charAssign(1)

button = [0, 0, 0, 0]
text = ''
password = password.PassTerminal("data.txt")
key_enable = 1
while True:
    button[0] = keyboard1.advCheck(keyboard1.key_nr[1], 1)
    button[1] = keyboard1.advCheck(keyboard1.key_nr[2], 2)
    button[2] = keyboard1.advCheck(keyboard1.key_nr[3], 3)
    button[3] = keyboard1.advCheck(keyboard1.key_nr[4], 4)

    for x in range(4):
        if button[x][1] != 0 and key_enable == 1:
            if button[x][0] == 'ok':
               check = password.tryPassword(text)
               lcd.clear()
               lcd.message = check[0]
               key_enable = 0
            else:
                text = text + button[x][0]

    if key_enable == 1:
        lcd.message = "Wpisz haslo:\n" + text




